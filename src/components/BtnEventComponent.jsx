import React, { Component } from 'react';
import PropTypes from 'prop-types';

class BtnEventComponent extends Component {
    constructor(props) {
        console.log('순서-constructor');
        super(props);//state 초기화
        this.state = { count: props.count }; //0 초기 값을 상위 컴포넌트에서 전송 받은 props 값으로 지정
        this.clickEvent = this.clickEvent.bind(this);//this를 바인딩하면, clickEvent함수에서 this를 사용할 수 있다.        
    }

    componentWillMount() { //클래스 마운트(렌더링) 전
        console.log('순서- componentWillMount');
    }
    clickEvent() { //사용자 지정 함수 추가
        //state 변경 let count = this.state.count; //prevState 내장변수를 사용하여 코드를 단축한다.
        this.setState(prevState => ({
            count: prevState.count + 1,
        }));
        //this.state.count = this.state.count + 1;
        //this.setState({count:this.state.count+1});
    }
    componentDidMount() {
        console.log('순서- componentDidMount');
    }

    componentWillReceiveProps(nextProps) { // 상위 컴포넌트의 props값이 변경 되었을 때 실행
        console.log('순서-componentWillReceiveProps');
        this.setState({count: nextProps.count}); //이 부분 추가
    }
    //static getDerivedStateFromProps(props, state) {return {count: props.count}}//처럼 사용할 수도 있지만 잘 사용 않함
    shouldComponentUpdate(nextProps, nextState) {
        console.log('순서-shouldComponentUpdate');
        return true; //화면 업데이트 생명주기 함수에 return 이 필요하다. 이 함수를 지우면 기본 값이 true 이다
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('순서-componentWillUpdate');
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('순서-componentDidUpdate');
    }

    componentWillUnmount() {
        console.log('순서- componentWillUnmount');
    }

    render() {
        console.log('순서- render ');
        return (
            <div>
                <p>카운트: {this.state.count}</p>
                <button onClick={this.clickEvent}>카운트 증가</button>
            </div>
        );
    }
}

BtnEventComponent.propTypes = {

};

export default BtnEventComponent;