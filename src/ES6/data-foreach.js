function es5Parse(params) { //es5 고전 for문(아래)
    var queryString = params.split('?'); //? 문자열로 분리한 배열로 반환한다.
    var chunks = queryString[1].split('&'); //배열로 반환된다.
    var result = {}; //빈 json 객체 생성
    for (var i = 0; i < chunks.length; i++) { //반복변수 i와 반복연산i++, 반복횟수제한 length 변수사용
        var parts = chunks[i].split('='); //배열로 반환된다.
        var key = parts[0];
        var value = parts[1];
        result[key] = value; //{ key:value } 생성됨
    }
    return result;
}

function es6Parse(params) { //var 로 해도 되지만, const상수, let변수를 사용한다.(아래)
    const queryString = params.split('?'); //? 문자열로 분리한 배열로 반환한다.
    const chunks = queryString[1].split('&'); //배열로 반환된다.
    let result = {}; //빈 json 객체 생성
    chunks.forEach(element => { //element에 chunks 배열의 인덱스 값이 자동으로 대입된다.
        const [key, value] = element.split('='); //배열값의 분해할당
        result[key] = value;
    });
    return result;
}
//영화진흥위원회 api URL 경로 확인 및 es6 사용 파싱 함수 실행
var apiUrl = 'http://kobis.or.kr/kobisopenapi/webservice/rest/boxoffice/searchDailyBoxOfficeList.json?key=f5eef3421c602c6cb7ea224104795888&targetDt=20120101';
//쿼리 스트링이란? 위 URL 경로에서 ?다음의 문자열을 쿼리 스트링 문자열이라고 한다. 쿼리스트링은 & 엔퍼센드 문자열로 개체(element)를 분리되고, 분리된 각 개체는 키=값 문자열을 갖는다.
console.log(es6Parse(apiUrl));
console.log(es5Parse(apiUrl));