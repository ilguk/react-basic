/*global kakao*/
import React, { useEffect, useState } from 'react'; //Component대신 useEffect, useState 사용
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

function KakaoMap(props) {
    //클래스의 state 상태변수값 초기화 대신 useState() 함수를 사용하여 초기화
    var [keyword, setKeyword] = useState('천안시');
    var [totalCount, setTotalCount] = useState(0);
    var [pageNo, setPageNo] = useState(1); //앞 변수가 js처리, 뒤 함수가 화면처리
    //[공통사항]함수에 function 예약어를 붙인다. this 클래스 객체를 지운다.(이후)
    function repeatPage(totalCount) { //검색된 전체 갯수를 10개씩 나누어 출력될 디자인 페이지를 구한다.
        var pagingNo = Math.ceil(totalCount / 10);
        var arr = [];
        for (var i = 1; i <= pagingNo; i++) {
            arr.push(
                <option key={i} value={i}>{i}</option>
            );
        }
        return arr;
    } //함수형 변수형태를 사용한다면 var 예약어를 붙인다(아래)
    var onPage = (e) => { //페이지 선택 이벤트 함수
        setPageNo(e.target.value);//화면처리
        pageNo = e.target.value;//js 처리
        var mapContainer = document.getElementById('map');
        removeAllChildNods(mapContainer);//기존 카카오맵 겍체 지우기
        getData();
    };
    function removeAllChildNods(el) { //기존 지도 지우기
        while (el.hasChildNodes()) {
            el.removeChild(el.lastChild);
        }//기술참조:https://apis.map.kakao.com/web/sample/keywordList/
    }
    function onSearch() { // 검색 버튼 이벤트 함수
        var mapContainer = document.getElementById('map');
        removeAllChildNods(mapContainer);//기존 카카오맵 겍체 지우기
        setPageNo(1);//화면처리
        pageNo = 1;//js처리
        getData();
    }
    function onChange(e) { // 검색어 수정 이벤트 함수
        setKeyword(e.target.value);//화면처리
        keyword = e.target.value;
    }
    function getData() {
        var url = 'https://bitter-mariel-kimilguk.koyeb.app/openapi/getdata?keyword='+this.state.keyword+'&pageNo='+this.state.pageNo;
        //var url = 'http://localhost:4000/openapi/getdata?keyword=' + keyword + '&pageNo=' + pageNo;;
        fetch(url, { method: 'get' }) //체인방식으로 실행. 장점은 줄 순서대로 각각 실행 결과가 마무리 된 후 다음 줄이 실행 된다.
            .then(response => response.json()) //응답데이터를 json 형태로 변환
            .then(contents => { //json으로 변환된 응답데이터인 contents 를 가지고 구현하는 내용
                totalCount = contents['response']['body']['totalCount']['_text'];//js처리
                setTotalCount(contents['response']['body']['totalCount']['_text']);//화면처리
                var positions = [];//배열 선언
                var jsonData;
                jsonData = contents['response']['body']['items'];
                console.log(jsonData);
                jsonData['item'].forEach((element) => {//람다식 사용 function(element) {}
                    positions.push(
                        {
                            content: "<div>" + element["csNm"]['_text'] + "</div>",//충전소 이름
                            latlng: new kakao.maps.LatLng(element["lat"]['_text'], element["longi"]['_text']) // 위도(latitude), 경도longitude)
                        }
                    );
                });
                //구현코드 이곳에 위 카카오 맵 샘플 js 코드를 그대로 입력한다.
                var index = parseInt(positions.length / 2);//배열은 인덱스순서 값을 필수로 가지고, 여기서는 반환 값의 개수로 구한다.
                console.log(jsonData["item"][index]["lat"]['_text']);
                //console.log(jsonData);
                var mapContainer = document.getElementById('map'), // 지도를 표시할 div  
                    mapOption = {
                        center: new kakao.maps.LatLng(jsonData["item"][index]["lat"]['_text'], jsonData["item"][index]["longi"]['_text']),
                        //center: new kakao.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
                        level: 10 // 지도의 확대 레벨
                    };
                var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
                for (var i = 0; i < positions.length; i++) {
                    // 마커를 생성합니다
                    var marker = new kakao.maps.Marker({
                        map: map, // 마커를 표시할 지도
                        position: positions[i].latlng // 마커의 위치
                    });

                    // 마커에 표시할 인포윈도우를 생성합니다 
                    var infowindow = new kakao.maps.InfoWindow({
                        content: positions[i].content // 인포윈도우에 표시할 내용
                    });

                    // 마커에 mouseover 이벤트와 mouseout 이벤트를 등록합니다
                    // 이벤트 리스너로는 클로저를 만들어 등록합니다 
                    // for문에서 클로저를 만들어 주지 않으면 마지막 마커에만 이벤트가 등록됩니다
                    kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
                    kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
                }

                // 인포윈도우를 표시하는 클로저를 만드는 함수입니다 
                function makeOverListener(map, marker, infowindow) {
                    return function () {
                        infowindow.open(map, marker);
                    };
                }

                // 인포윈도우를 닫는 클로저를 만드는 함수입니다 
                function makeOutListener(infowindow) {
                    return function () {
                        infowindow.close();
                    };
                }

            })
            .catch((err) => console.log('에러: ' + err + '때문에 접속할 수 없습니다.'));// then(content) 끝
    }// getData() 끝
    useEffect(() => { // 생명주기 중 초기 화면 렌더링 후 실행함수 대신 사용(람다식으로 변경)
        getData();
    }, []);//keyword 를 입력하면 실시간으로 바뀐다. 우리는 버튼액션이라서 제외

    //render() 함수 대신에 return() 함수로 끝낸다. render 부분 지운다.
    //props-상태 값이 바뀌면 html을 그리는 함수 즉, render 자동으로 재 실행됨
    //아래  div 내 에 위 카카오 맵 샘플 js 의 html 부분을 복사해서 사용 단, style은 JSX 전용으로 해야 하는것에 주의한다
    return (
        <div>
            <h2><a href="/">함수형 전기차 충전소 위치</a></h2>
            <span>충전소 도시 검색(아래 검색할 시를 입력하고 검색 버튼을 누른다.)</span>
            <input className="form-control" type="text" id="keyword" onChange={onChange} value={keyword} />
            <input className="form-control btn btn-primary" type="button" onClick={onSearch} value="검색" />
            <span>페이지이동(아래 번호를 선택하면 화면이 전환된다.)</span><select className="form-select" id="pageNo" onChange={onPage} value={pageNo}>
                {repeatPage(totalCount)}
            </select>
            <Link to="/"><button className="form-control btn btn-primary" id="btnHome">홈으로</button></Link>
            <div id="map" style={{ width: "100%", height: "70vh" }}></div>
        </div >
    );//return 함수 끝
}//함수 끝
KakaoMap.propTypes = {
};
export default KakaoMap;