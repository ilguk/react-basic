//componentWillMount() 생명주기 함수 대신에 useEffect() 함수를 사용한다.
import React, { useEffect, useState } from 'react';
//ChildComponent.jsx 컴포넌트에 아래 붉은 코드 부분 추가
import PropTypes from 'prop-types'; //리액트에 내장된 유효성 검사 라이브러리를 불러온다.

function ChildComponent(props) {
    //constructor() 생성자는 함수형에서는 사용하지 않기 때문에 초기화는 useState()함수에서 처리(아래)
    //state예약변수 대신 useState함수로 state변수와, setState함수를 초기화(아래)
    const [state, setState] = useState({ loading: true, formData: 'no data' });
    function clickEvent() {
        const data = 'new data';
        const { formData } = state;//초기formData변수만 바인딩
        //setState()리액트 내장함수로 state 변경
        setState({
            loading: false,
            formData: data + formData,
        });
        //this.state.loading은 현재 true 이다. 클래스가 아니라서 this가 필요없다.
        //this.clickEvent = this.clickEvent.bind(this);
        console.log('loading 변수값', state.loading);
        //이후 호출될 render() 함수에서 this.state.loading은 false로 동기화된다.
    }
    useEffect(() => {
        //4초 후에 clickEvent 함수를 호출(아래)
        setTimeout(clickEvent, 4000);
    }, []);//componentWillMount() 대신 사용 , 두 번째 인자 []가 비었을 때는 1번만 실행된다.
    //[state] 처럼 하면 state가 변경 될 때마다 즉, 4초마다 실행된다.([]는변경 의존 조건이라 한다.)
    const {
        name, boolValue, numValue, arrayValue, objValue, nodeValue, funcValue,
    } = props;
    const message = boolValue ? '불린 값 있음' : '불린 값 없음';
    return (
        <div>
            {/*주석,state 데이터는 this.state로 접근 가능하다. String()은 형 변환 함수*/}
            <p>
                로딩중: {String(state.loading)}
                결과: {state.formData}
            </p>
            작업 컴포넌트 추가 {name}<br />
            {message} {numValue}<br />
            {arrayValue} {objValue.id}<br />
            {nodeValue} {funcValue(5, 5)}
            {props.children} {/* 노드태그출 */}
        </div>
    );
}
//자료형을 선언하는 코드(아래)
ChildComponent.propTypes = { //소문자로 시작하는 변수 명 주의 porpTypes 
    name: PropTypes.string,
    boolValue: PropTypes.bool,
    numValue: PropTypes.number.isRequired,
    arrayValue: PropTypes.array,
    objValue: PropTypes.object,
    nodeValue: PropTypes.node,
    funcValue: PropTypes.func,
}
ChildComponent.defaultProps = {
    numValue: 3,
} //isRequired 오류는 위 코드로 해결이 가능하다.즉, 전송 값이 없으면, 초기 값을 지정 할 수 있다.
export default ChildComponent;