import logo from './logo.svg';
import './App.css';
import ChildComponent from './components/ChildComponentFunc';//외부 컴포넌트 불러오기
import BtnEventComponent from './components/BtnEventComponentFunc';
import React, { useState } from 'react'; //리액트 내장 모듈에서 useState 객체 import
import { Link } from "react-router-dom";
function App() {
  //함수형 컴포넌트에서는 클래스의 state 예약변수 대신 useState함수로 변수 초기화(아래)
  const [state, setState] = useState({ count: 10 });
  const resetEvent = () => { //기존 값+10으로 리셋하기
    //state 변경 let count = state.count;
    setState(prevState => ({
      count: prevState.count + 10,
    }));
  }
    const funcAdd = (x, y) => { return x + y; };
    return (
      <div className="App">
        {/* <ClassKakaoMap />
        <BtnEventComponent count={state.count} />
        <button onClick={resetEvent}>{state.count + 10}으로 초기화</button>
        <ChildComponent name="문자메세지"
          boolValue={true}		//불린 형
          //numValue={2}		 //숫자 형
          arrayValue={[1, 2, 3]}	 //배열 형
          objValue={{ id: '아이디', age: 12 }} //json객체 형
          nodeValue={<h1>태그노드2</h1>}//HTML태그노드(태그 열고~닫기 영역을 1개의 노드라 한다.)
          funcValue={funcAdd} //함수 형
        >
          <h2>자식노드</h2> 
        </ChildComponent> */}
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>안녕하세요 리액트 프로그래밍^^</p>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <Link to="/classkakaomap"><button id="btnHome">클래스형 카카오 맵</button></Link>
          <Link to="/functionkakaomap"><button id="btnHome">함수형 카카오 맵</button></Link>
        </header>
      </div>
    );
  }

  export default App;
