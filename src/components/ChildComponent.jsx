import React, { Component } from 'react';
//ChildComponent.jsx 컴포넌트에 아래 붉은 코드 부분 추가
import PropTypes from 'prop-types'; //리액트에 내장된 유효성 검사 라이브러리를 불러온다.

class ChildComponent extends Component {
    constructor(props) { //생성자에서 state 객체 초기화
        super(props);//부모Component클래스의 props 객체 초기화
        this.state = {//state예약어로 객체변수초기화(아래)
            loading: true,
            formData: 'no data',
        };
        //이후 콜백(clickEvent) 함수 내부에서 this.state변수를 사용하기 위해 bind(this)로 바인딩 필수
        this.clickEvent = this.clickEvent.bind(this);//this는 ChildComponent 영역을 말한다.
    }
    componentWillMount() { // 리액트 생명주기 중 클래스 마운트(렌더링) 전 콜백(Call Back) 함수 자동 실행됨
        setTimeout(this.clickEvent, 4000); //4초 후에 clickEvent 함수를 호출
    }
    clickEvent() {
        const data = 'new data';
        let { formData } = this.state;//초기 formData 변수만 바인딩
        //setState()리액트 내장함수로 state 변경, 이 함수로 변경해야지 render함수가 새로 호출 된다.
        /*this.setState({
            loading: false,
            formData: data + formData,
        });*/ //this.state.loading은 render함수가 실행 전에는 true 이다. 크롬 개발자도구F12-콘솔 창에서 확인해 본다.
        this.state.loading = false;
        this.state.formData = data + formData;
        this.forceUpdate();
        //setState대신 직접 접근해서 변경하였기 때문에 여기서 바로 false가 출력된다.(이전과 다르다!)
        console.log('loading 변수값', this.state.loading);
        //이후 호출될 render() 함수에서 this.state.loading은 false로 동기화된다.
    }
    render() {
        //const name = this.props.name; //props속성명과 변수 명이 같다면 아래처럼 간단하게 코딩
        const {
            name, boolValue, numValue, arrayValue, objValue, nodeValue, funcValue,
        } = this.props; //this는 현재 클래스 영역이다.
        const message = boolValue ? '불린 값 있음' : '불린 값 없음';
        return (
            <div>
                {/*주석,state 데이터는 this.state로 접근 가능하다. String()은 형 변환 함수*/}
                <p>
                    로딩중: {String(this.state.loading)}
                    결과: {this.state.formData}
                </p>
                작업 컴포넌트 추가 {name}<br />
                {message} {numValue}<br />
                {arrayValue} {objValue.id}<br />
                {nodeValue} {funcValue(5, 5)}
                {this.props.children} {/* 노드태그출 */}
            </div>
        );
    }
}
//자료형을 선언하는 코드(아래)
ChildComponent.propTypes = { //소문자로 시작하는 변수 명 주의 porpTypes 
    name: PropTypes.string,
    boolValue: PropTypes.bool,
    numValue: PropTypes.number.isRequired,
    arrayValue: PropTypes.array,
    objValue: PropTypes.object,
    nodeValue: PropTypes.node,
    funcValue: PropTypes.func,
}
ChildComponent.defaultProps = {
    numValue: 3,
} //isRequired 오류는 위 코드로 해결이 가능하다.즉, 전송 값이 없으면, 초기 값을 지정 할 수 있다.

export default ChildComponent;